package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examen.Api.Api;
import com.example.examen.Api.Servicios.ServicioPeticion;
import com.example.examen.models.RegistrarUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearCuentaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);

        Button btnCrearCuenta =(Button)findViewById(R.id.btnCrearCuenta);
        btnCrearCuenta.setOnClickListener(new View.OnClickListener() {
            EditText correo1 =(EditText)findViewById(R.id.correo1);
            EditText password1=(EditText)findViewById(R.id.password1);
            @Override
            public void onClick(View v) {
                ServicioPeticion service = Api.getApi(CrearCuentaActivity.this).create(ServicioPeticion.class);
                Call<RegistrarUsuario> registrarCall =  service.registrarusuario(correo1.getText().toString(),password1.getText().toString());
                registrarCall.enqueue(new Callback<RegistrarUsuario>() {
                    @Override
                    public void onResponse(Call<RegistrarUsuario> call, Response<RegistrarUsuario> response) {
                        RegistrarUsuario peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(CrearCuentaActivity.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(CrearCuentaActivity.this,MainActivity.class));
                            Toast.makeText(CrearCuentaActivity.this, "Muy bien Dato Registrado", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(CrearCuentaActivity.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<RegistrarUsuario> call, Throwable t) {
                        Toast.makeText(CrearCuentaActivity.this, "Erro :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
