package com.example.examen.Api.Servicios;

import com.example.examen.models.InformeFechas;
import com.example.examen.models.Login;
import com.example.examen.models.RegistrarUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistrarUsuario> registrarusuario(@Field("username") String correo, @Field("password") String contrasenia);
    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);
    @FormUrlEncoded
    @POST("api/informacionFechas")
    Call<InformeFechas>InformeFecha(@Field("token")String token);
}

